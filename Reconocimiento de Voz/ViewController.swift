//
//  ViewController.swift
//  Reconocimiento de Voz
//
//  Created by Victor Hugo Benitez Bosques on 25/09/16.
//  Copyright © 2016 Victor Hugo Benitez Bosques. All rights reserved.
//  AVFundation ya viene importado por defecto en IOS 10

import UIKit
import Speech

class ViewController: UIViewController, AVAudioRecorderDelegate {

    @IBOutlet var textView: UITextView! //Donde se hara la trasncripcion de voz a texto
    var audioRecordingSesion : AVAudioSession!
    var audioFileName : String = "audio-recordered.m4a"
    var audioRecorder :AVAudioRecorder!  //Objeto encargado de grabar
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //recognizeSpeech()
        recordingAudioSetup() //Solicita los permisos uso del microfono
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func recognizeSpeech(){
        SFSpeechRecognizer.requestAuthorization { (authStatus) in  //se verifica el estatus de la autorizacion para reconocimiento de voz
            
            if authStatus == SFSpeechRecognizerAuthorizationStatus.authorized{  //Si esta autorizada se ejecuta el siguiente codigo
                
                let recognizer = SFSpeechRecognizer() //Se encargara del reconocimiento de voz del archivo mp3
                let request = SFSpeechURLRecognitionRequest(url: self.directoryURL()!)  //Se realiza la peticion de a que archivo se realizara el reconocimeinto de voz
                
                recognizer?.recognitionTask(with: request, resultHandler: { (result, error) in
                    if let error = error{
                        print("Algo ha ido mal: \(error.localizedDescription)")
                    }else{
                        self.textView.text = String(describing: result?.bestTranscription.formattedString)
                    }
                })
                
            }else{
                print("No tengo acceso para acceder al Speech Framework")
            }
            
        }
        
    }
    
    func recordingAudioSetup() {  //1. PEDIRA PERMISOS DE USO DE MICROFONO AL USUARIO (MODAL) Reealizar la configuracion de grabado de audio
        audioRecordingSesion = AVAudioSession.sharedInstance()  //Se indica que el audio a grabar sera del microfono campartido del dispositivo
        
        do{
            try audioRecordingSesion.setCategory(AVAudioSessionCategoryRecord) //indicamos que usaremso el microfono para grabar audio
            try audioRecordingSesion.setActive(true)  //Activar la sesion de grabado indicando que estamos listo para grabar
            
            //Permisos para grabar audio
            audioRecordingSesion.requestRecordPermission({ [unowned self] (allowed: Bool) in // unowned self : para que dentro de el handler no se anteponga a las variables sel.atributo
                
                if allowed{
                    //Hay que empezar a grabar pues tenemos permisos para hacerlo
                    /* como los archivos de audio son fisicos guardador en una carpeta especifica hay que indicar la estructura de carpetas donde se tendra el archivo*/
                    self.startRecording()
                    
                }else{
                
                    print("Necesito permisos para utilizar el microfono")
                }
            })
            
        
        }catch{
            print("Error al intentar grabar audio")
        }
    }
    
    
    func directoryURL() -> URL? { //2.1 SE CONFIGURA LA ESTRUCTURA DE DIRECTORIOS DONDE SE GUARDARA EL ARCHIVO DE AUDIO Se generara la estructura de carpetas donde se guardara el archivo de audio del microfono
        let fileManager = FileManager.default //gestor de archivo : buscara donde guardar el archivo
        let urls = fileManager.urls(for: .documentDirectory, in: .userDomainMask)  //Indicamos que el usuario lo guardara en el directorio de archivos
        let documentsDirectory = urls[0] as NSURL  //accedemos a la primera url donde guardara el archivo
        
        return documentsDirectory.appendingPathComponent(audioFileName) as URL?
    }
    
    
    func startRecording() { // 2. SI AUTORIZA PERMISOS CONFIGURA EL FORMATO DE AUDIO LLAMA AL METODO  directoryURL()
        let settings = [AVFormatIDKey : Int(kAudioFormatMPEG4AAC),  //AVSampleRateKey numero de muestras por segundo frecuencia
                       AVSampleRateKey : 12000.0,
                       AVNumberOfChannelsKey : 1 as NSNumber,
                       AVEncoderAudioQualityKey : AVAudioQuality.high.rawValue] as [String : Any]
        
        do{
            audioRecorder = try AVAudioRecorder(url: directoryURL()!, settings: settings)
            audioRecorder.delegate = self  //podemos observar cambios al delegar a la vista asignamos a la vista AVAudioRecorderDelegate
            audioRecorder.record()  //Empezara a grabar
            
            
            //Crearemos un timer
            Timer.scheduledTimer(timeInterval: 10.0, target: self, selector: #selector(self.stopRecording), userInfo: nil, repeats: false) //3. SE LANZA A LOS 10 SEGUNDO EL METODO stopRecording funcion que ejecua un determinado selector(Metodo indicando los parametros)
            
        }catch{
            print("No se ha podido grabar el audio correctamente")
        }
        
        
    }
    
    func stopRecording() { //4. PARA LA GRABACION DE AUDIO DEL MICROFONO Y LANZA EL METODO recognizeSpeech() realiza el reconocimiento del audio para pasarlo al textView
        audioRecorder.stop()
        audioRecorder = nil //liberar espacio en memoria y no ocupar la bateria
        
        Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(recognizeSpeech), userInfo: nil, repeats: false)
        
    }


}

